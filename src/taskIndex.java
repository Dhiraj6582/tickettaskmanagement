
import java.awt.HeadlessException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import net.proteanit.sql.DbUtils;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author DELL
 */
public class taskIndex extends javax.swing.JFrame {

    /**
     * Creates new form taskIndex
     */
    Connection conn;
    PreparedStatement ps = null;
    ResultSet rs = null;
    int id;
    String role;
    String sql;
    String username;
    String emailFrom, reassignedEmail, acceptTaskUserEmail;
    public taskIndex(int id, String role, String name) {
        initComponents();
        jComboBox1.addItem(name);
        jComboBox1.addItem("Log Out");
        conn = MySQLConnect.getConnection();
        
        this.id = id;
        this.role = role;
        this.username = name;
        if("leader".equals(role))
        {
            sql = "Select id,name,descripition,assigned_to, assigned_by,status,deadline from tasks";  
            btngiveup.setVisible(false);
            btncompleted.setVisible(false);
        }
        else
        {
            sql = "Select id,name,descripition,assigned_to,assigned_by,status,deadline from tasks where assigned_to =" +id;
            btnaccept.setVisible(false);
            btnreject.setVisible(false);
            backbutton.setVisible(false);
        }
        updateTableData(sql);
             txtSearch.getDocument().addDocumentListener(new DocumentListener(){
            @Override
            public void insertUpdate(DocumentEvent e){
                searchFilter();
            }
            
            @Override
            public void removeUpdate(DocumentEvent e){
                searchFilter();
            }
            
            @Override
            public void changedUpdate(DocumentEvent e){
                
            }
        });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        back = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtabledata = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        btnreject = new javax.swing.JButton();
        btncompleted = new javax.swing.JButton();
        btnaccept = new javax.swing.JButton();
        backbutton = new javax.swing.JButton();
        btngiveup = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        txtSearch = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        limitvalue = new javax.swing.JComboBox<>();
        jComboBox1 = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        back.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2));

        jtabledata.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jtabledata.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jtabledata.setRowHeight(24);
        jtabledata.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jtabledataMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jtabledata);

        btnreject.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnreject.setIcon(new javax.swing.ImageIcon("C:\\Users\\DELL\\Downloads\\reject-20.png")); // NOI18N
        btnreject.setText("Reject");
        btnreject.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnrejectActionPerformed(evt);
            }
        });

        btncompleted.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btncompleted.setIcon(new javax.swing.ImageIcon("C:\\Users\\DELL\\Downloads\\accept-20.png")); // NOI18N
        btncompleted.setText("Completed");
        btncompleted.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncompletedActionPerformed(evt);
            }
        });

        btnaccept.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnaccept.setIcon(new javax.swing.ImageIcon("C:\\Users\\DELL\\Downloads\\accept-20.png")); // NOI18N
        btnaccept.setText("Accept");
        btnaccept.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnacceptActionPerformed(evt);
            }
        });

        backbutton.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        backbutton.setIcon(new javax.swing.ImageIcon("C:\\Users\\DELL\\Downloads\\9541818211553167201-20.png")); // NOI18N
        backbutton.setText("Back");
        backbutton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backbuttonActionPerformed(evt);
            }
        });

        btngiveup.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btngiveup.setIcon(new javax.swing.ImageIcon("C:\\Users\\DELL\\Downloads\\reject-20.png")); // NOI18N
        btngiveup.setText("GiveUp");
        btngiveup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btngiveupActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(backbutton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnaccept)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnreject)
                .addGap(82, 82, 82)
                .addComponent(btncompleted)
                .addGap(18, 18, 18)
                .addComponent(btngiveup)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btncompleted)
                    .addComponent(btngiveup)
                    .addComponent(btnaccept)
                    .addComponent(btnreject)
                    .addComponent(backbutton))
                .addContainerGap())
        );

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("Search");

        txtSearch.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setIcon(new javax.swing.ImageIcon("C:\\Users\\DELL\\Downloads\\search.png")); // NOI18N

        limitvalue.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        limitvalue.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "5", "10", "25", "50" }));
        limitvalue.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                limitvalueActionPerformed(evt);
            }
        });

        jComboBox1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout backLayout = new javax.swing.GroupLayout(back);
        back.setLayout(backLayout);
        backLayout.setHorizontalGroup(
            backLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(backLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1147, Short.MAX_VALUE)
                .addContainerGap())
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(backLayout.createSequentialGroup()
                .addGap(41, 41, 41)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 255, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addGap(96, 96, 96)
                .addComponent(limitvalue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(112, 112, 112))
        );
        backLayout.setVerticalGroup(
            backLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(backLayout.createSequentialGroup()
                .addContainerGap(27, Short.MAX_VALUE)
                .addGroup(backLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel2)
                    .addGroup(backLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(backLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(limitvalue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 261, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(34, Short.MAX_VALUE)
                .addComponent(back, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(34, 34, 34))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(54, 54, 54)
                .addComponent(back, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(65, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


     private void searchFilter(){
        String key = txtSearch.getText();
        String searchSql;
        if(!key.equals("")){
            if("leader".equals(role))
            {
                searchSql = "Select id,name,descripition,assigned_to, assigned_by,status,deadline from tasks where name like '%" + key + "%' or status like '%"+ key + "%'";
            }
            else
            {
                searchSql = "Select id,name,descripition,assigned_to,assigned_by,status,deadline from tasks where name like '%" + key + "%' or status like '%"+ key + "%' And assigned_to =" +id;
            }
            //searchSql = "SELECT * FROM employee WHERE name like '%" + key + "%' or Contact like '%" + key + "%'"; 
        }else{
            if("leader".equals(role))
            {
                searchSql = "Select id,name,descripition,assigned_to, assigned_by,status,deadline from tasks";
            }
            else
            {
                searchSql = "Select id,name,descripition,assigned_to,assigned_by,status,deadline from tasks where assigned_to =" +id;
            }
                
        }
        updateTableData(searchSql);
    }
    private void giveuptaskassigned(String sql, String task_id)
    {
        try
        {
            ps = conn.prepareStatement(sql);
            rs =  ps.executeQuery();
            System.out.println("Id="+task_id);
            if(rs.next())
            {
                int give_up_task_assigned_to_another_member = rs.getInt(1);
                reassignedEmail = rs.getString("email");
                System.out.println("Give up ="+give_up_task_assigned_to_another_member);
                insertTask(give_up_task_assigned_to_another_member, task_id);
            }
        }
        catch(SQLException e)
        {
            JOptionPane.showMessageDialog(null, "DB Issue :" + e.getMessage());
        }
    }
    
    private void insertTask(int user_id, String task_id)
    {
       
        System.out.println("User="+user_id+"Task="+task_id);
        String sql = "Select * from tasks where id="+task_id;
        try
        {
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            if(rs.next())
            {
                String insertSqlQuery = "Insert into tasks (name, descripition,priority, assigned_to, assigned_by, deadline, status) values(?,?,?,?,?,?,?)";
                try
                {
                    ps = conn.prepareStatement(insertSqlQuery);
                    ps.setString(1, rs.getString(2));
                    ps.setString(2, rs.getString(3));
                    ps.setString(3, rs.getString(4));
                    ps.setInt(4, user_id);
                    ps.setInt(5, rs.getInt("assigned_by"));
                    ps.setString(6, rs.getString("deadline"));
                    ps.setString(7, "assigned");
                    ps.execute();
                    
                    JOptionPane.showMessageDialog(null, "Task has been assigned to another member");   
                    
                    SendEmail sendEmail = new SendEmail();
                    sendEmail.sendMail("dhirajwadhwani789@gmail.com",reassignedEmail,"New Task", "A new Task has been added");
                }
                catch(SQLException | HeadlessException ex)
                {
                    JOptionPane.showMessageDialog(null, "DB Issue :" + ex.getMessage());
                }
            }    
        }
        catch(SQLException e)
        {
            JOptionPane.showMessageDialog(null, "DB Issue :" + e.getMessage());
        }
    }
    private void btngiveupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btngiveupActionPerformed
        // TODO add your handling code here:
        int getSelectedRow = jtabledata.getSelectedRow();
       
        String selectedEmpId =  jtabledata.getModel().getValueAt(getSelectedRow, 0).toString();
        String sql = "Update tasks SET status = ? where id = "+selectedEmpId;
        String emailSql = "Select email from users where id = "+id;
        try
        {
            ps = conn.prepareStatement(sql);
            ps.setString(1, "Member has left the task");
            ps.execute();
            
            PreparedStatement ps1 = conn.prepareStatement(emailSql);
            rs = ps1.executeQuery();
            
            if(rs.next()){
                emailFrom = rs.getString("email");
            }
            
            
            
            SendEmail sendEmail = new SendEmail();
            sendEmail.sendMail(emailFrom,"dhirajwadhwani789@gmail.com", "Give Up Task", "A Task has been give up");
            
            String sqlQueryData =  "Select id,name,descripition,assigned_to,assigned_by,status,deadline from tasks where assigned_to =" +id;
            updateTableData(sqlQueryData);
            
            System.out.println("Id="+id);
            //String maxtaskId = "SELECT id, email from users where role = 'member' and task_completed = (SELECT max(task_completed) from users WHERE role='member' and users.id <>  " + id +")";
            String chsql = "" +"Select id,email from users where role = 'member' and task_completed - give_up_tasks = (SELECT max(task_completed - give_up_tasks) from users where role = 'member') and users.id <> "+ id;
            giveuptaskassigned(chsql, selectedEmpId);
            
            JOptionPane.showMessageDialog(null, "Record Updated");
        }
        catch(SQLException e)
        {
             JOptionPane.showMessageDialog(null, "DB Issue :" + e.getMessage());
        }
        System.out.println(sql);
    }//GEN-LAST:event_btngiveupActionPerformed

    private void jtabledataMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtabledataMouseClicked
        // TODO add your handling code here:
        int getSelectedRow = jtabledata.getSelectedRow();
        if(getSelectedRow != -1)
        {
            btngiveup.setEnabled(true);
            btncompleted.setEnabled(true);
        }
        String selectedEmpId =  jtabledata.getModel().getValueAt(getSelectedRow, 0).toString();
        try
        {
            String sql = "SELECT * FROM tasks where id = "+ selectedEmpId;
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            if(rs.next())
            {
                String status = rs.getString("status");
                if("assigned".equals(status) && "member".equals(role))
                {
                    btngiveup.setEnabled(true);
                    btncompleted.setEnabled(true);
                }
                else if("Member has been completed from client side".equals(status) && "leader".equals(role))
                {
                
                    btnaccept.setEnabled(true);
                    btnreject.setEnabled(true);
                }
                else
                {
                    btngiveup.setEnabled(false);
                    btncompleted.setEnabled(false);
                    btnaccept.setEnabled(false);
                    btnreject.setEnabled(false);
                }
            }
        }
        catch(Exception e)
        {
            
        }    
        
    }//GEN-LAST:event_jtabledataMouseClicked

    private void btncompletedActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncompletedActionPerformed
        // TODO add your handling code here:
        int getSelectedRow = jtabledata.getSelectedRow();
       
        String selectedTaskId =  jtabledata.getModel().getValueAt(getSelectedRow, 0).toString();
        String selectedEmpId =   jtabledata.getModel().getValueAt(getSelectedRow, 3).toString();
        String completedsql = "Update tasks SET status = ?, completed_at = ? where id = "+selectedTaskId;
        
        String emailSql = "Select email from users where id = "+id;
        try
        {
            Date date= new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String formattedDate = sdf.format(date);
            
            ps = conn.prepareStatement(completedsql);
            ps.setString(1, "Member has been completed from client side");
            ps.setString(2, formattedDate);
            ps.execute();
            String sqlQueryData =  "Select id,name,descripition,assigned_to,assigned_by,status,deadline from tasks where assigned_to =" +id;
            updateTableData(sqlQueryData);
            
            PreparedStatement ps1 = conn.prepareStatement(emailSql);
            rs = ps1.executeQuery();
            
            if(rs.next()){
                emailFrom = rs.getString("email");
            }
            
            System.out.println(emailFrom);
            SendEmail sendEmail = new SendEmail();
            sendEmail.sendMail(emailFrom,"dhirajwadhwani789@gmail.com", "Task Status", "A Task has been completed");
            
            JOptionPane.showMessageDialog(null, "Record Updated");
            
        }
        catch(SQLException | HeadlessException e)
        {
            JOptionPane.showMessageDialog(null, "DB Issue :" + e.getMessage());
        }
    }//GEN-LAST:event_btncompletedActionPerformed

    private void btnacceptActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnacceptActionPerformed
        // TODO add your handling code here:
         int getSelectedRow = jtabledata.getSelectedRow();
         String selectedTaskId =  jtabledata.getModel().getValueAt(getSelectedRow, 0).toString();
         String selectedEmpId =   jtabledata.getModel().getValueAt(getSelectedRow, 3).toString();
         System.out.println(selectedEmpId);
         String taskacceptsql = "Update tasks set status = ? where id = "+selectedTaskId;
         String taskcompleted = "Select task_completed from users where id ="+selectedEmpId;
         String emailSql = "Select email from users where id = "+selectedEmpId;
         try
         {
            ps = conn.prepareStatement(taskcompleted);
            rs = ps.executeQuery();
            if(rs.next())
            {
                int tasks = rs.getInt("task_completed");
                tasks = tasks+1;
                String updatetaskcompletedsql = "update users set task_completed=? where id="+selectedEmpId;
                try
                {
                    ps = conn.prepareStatement(updatetaskcompletedsql);
                    ps.setInt(1, tasks);
                    ps.execute();
                    String sqlQueryData = "Select id,name,email,role,task_completed,status from users where role= 'member'";
                    updateTableData(sqlQueryData);
                    try
                    {
                        ps = conn.prepareStatement(taskacceptsql);
                        ps.setString(1, "Task has been resolved");
                        ps.execute();
                        String sqlQueryData1 = "Select id,name,descripition,assigned_to, assigned_by,status,deadline from tasks";
                        updateTableData(sqlQueryData1);
                        
                    }
                    catch(Exception ex)
                    {
                        
                    }    
                    
                     PreparedStatement ps1 = conn.prepareStatement(emailSql);
                     rs = ps1.executeQuery();
            
                     if(rs.next()){
                        acceptTaskUserEmail = rs.getString("email");
                    }
                     
                    
                    SendEmail sendEmail = new SendEmail();
                    sendEmail.sendMail("dhirajwadhwani789@gmail.com",acceptTaskUserEmail, "Task Status", "Congratulations!Your Task has been resolved");
                }
                catch(Exception e)
                {
                    
                }
                
            } 
            JOptionPane.showMessageDialog(null, "Record Updated");
         }   
         catch(SQLException e)
         {
              JOptionPane.showMessageDialog(null, "DB Issue :" + e.getMessage());
         }
         
    }//GEN-LAST:event_btnacceptActionPerformed

    private void btnrejectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnrejectActionPerformed
        // TODO add your handling code here:
         int getSelectedRow = jtabledata.getSelectedRow();
       
        String selectedEmpId =  jtabledata.getModel().getValueAt(getSelectedRow, 0).toString();
    }//GEN-LAST:event_btnrejectActionPerformed

    private void backbuttonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backbuttonActionPerformed
        // TODO add your handling code here:
        new EmployeeIndex(id, role, username).setVisible(true);
    }//GEN-LAST:event_backbuttonActionPerformed

    private void limitvalueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_limitvalueActionPerformed
        // TODO add your handling code here:
        String limit;
        limit = (String) limitvalue.getSelectedItem();
        int selectedLimit = Integer.parseInt(limit);
        System.out.println(limit);
        if("leader".equals(role))
                sql = "Select id, name, descripition, assigned_to, assigned_by,status, deadline from tasks Limit "+selectedLimit;
        else
                sql = "Select id, name, descripition,assigned_to, assigned_by, status, deadline from tasks Where assigned_to = " +id+" Limit "+selectedLimit;
        System.out.println(sql);
        updateTableData(sql);
    }//GEN-LAST:event_limitvalueActionPerformed

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
        // TODO add your handling code here:
        if(jComboBox1.getSelectedItem() != "Log Out") {
        } else {
            System.out.println("Log out");
            this.dispose();
            new Login().setVisible(true);
        }
    }//GEN-LAST:event_jComboBox1ActionPerformed

    /**
     * @param args the command line arguments
     */
    
    private DefaultTableModel getMyTableModel(TableModel dtm)
    {
        int nrows;
        nrows = dtm.getRowCount();
        int ncols = dtm.getColumnCount();
        Object[][] tabledata = new Object[nrows][ncols];
        for(int i =0;i<nrows;i++)
        {
            for(int j = 0;j<ncols;j++)
                tabledata[i][j] = dtm.getValueAt(i,j);
        }
        
        String[] colsHead = {"Id", "Name", "Description","Assigned To", "Assigned By", "Status", "Deadline"};
        DefaultTableModel myModel = new DefaultTableModel(tabledata, colsHead){
            @Override
            public boolean isCellEditable(int row, int col){
                return false;
            }  
        };
        return myModel;
    }
    
     private void updateTableData(String sql)
    {
        try
        {
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
           // System.out.println(rs);
            jtabledata.setModel(getMyTableModel(DbUtils.resultSetToTableModel(rs)));
        }
        catch(SQLException e)
        {
            JOptionPane.showMessageDialog(null, "Issue:" + e.getMessage());
        }
    }
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(taskIndex.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(taskIndex.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(taskIndex.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(taskIndex.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
              //  new taskIndex().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel back;
    private javax.swing.JButton backbutton;
    private javax.swing.JButton btnaccept;
    private javax.swing.JButton btncompleted;
    private javax.swing.JButton btngiveup;
    private javax.swing.JButton btnreject;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jtabledata;
    private javax.swing.JComboBox<String> limitvalue;
    private javax.swing.JTextField txtSearch;
    // End of variables declaration//GEN-END:variables
}
